/*
 * Genuino 101 node for RBSS
 * Mikko Mäkelä
 * 
 * detectionCharacteristic values map to:
 * 0 = No alarm
 * 1 = New alarm
 * 2 = Recording (set by central)
 * 3 = Alarm off
 * 
 * sensorCharacteristic values map to:
 * 0 = No alarms
 * 1 = Sensor 1 (PIR)
 * 2 = Sensor 2 (GYRO)
 * 3 = Sensors 1 + 2
 * 4 = Sensor 3 (TEMP)
 * 5 = Sensors 3 + 1
 * 6 = Sensors 3 + 2
 * 7 = Sensors 3 + 2 + 1 
 * 
 * TODO:
 * 
 * Connect to nearby genuinoService
 * Detect connection problems and use nearest device as a gateway (change bit mapping on states)
 * ...
 * 
 * Uses libraries CurieBLE for Bluetooth and Adafruit DHT11 for DHT11-temperature sensor:
 * 
 * https://github.com/01org/corelibs-arduino101/tree/master/libraries/CurieBLE
 * https://github.com/adafruit/DHT-sensor-library
 * 
 */
 
#include <CurieBLE.h>
#include <CurieTimerOne.h>
#include <DHT.h>

#define PIR 4
#define TEMP 5

// Define temperature alert limits
const float MAX_TEMP = 35.0;
const float MIN_TEMP = 12.0;

// Define temperature averaging array length - bigger array needs more 30s inputs to deviate enough to reach limits
const int TEMP_ARRAY_LENGTH = 5;

// Define "suspicious" movement time used to trigger PIR alarm
const long SUSPICIOUS_MOVEMENT_TIME = 30000;

// Define cooldown time from alarms
const long COOLDOWN_TIME = 60000;

float tempArray[TEMP_ARRAY_LENGTH];
float currentTempAverage;

DHT tempSensor(TEMP, DHT11);

BLEPeripheral blePeripheral;
BLEService genuinoService("a699b6ce-3b10-401a-834f-d1cee3000001");

// Create characteristics for BLE service (Detection state, alarming sensors, temperature + humidity data)
BLEIntCharacteristic detectionCharacteristic("a699b6ce-3b10-401a-834f-d1cee304771f", BLERead | BLEWrite);
BLEIntCharacteristic sensorCharacteristic("a699b6ce-3b10-401a-834f-d1cee304772f", BLERead);
BLEFloatCharacteristic tempCharacteristic("a699b6ce-3b10-401a-834f-d1cee304773f", BLERead);
BLEFloatCharacteristic tempAvgCharacteristic("a699b6ce-3b10-401a-834f-d1cee304774f", BLERead);
BLEFloatCharacteristic humidCharacteristic("a699b6ce-3b10-401a-834f-d1cee304775f", BLERead);
BLECharCharacteristic recordCharacteristic("a699b6ce-3b10-401a-834f-d1cee304776f", BLERead | BLEWrite);

// Time variables to record detection times
unsigned long lastDetectionTime = 0;

// Variables to store characteristic states
int detectionState = 0;
int sensorState = 0;

// Genuino setup function
void setup() {

  // Start serial (for debugging printouts)
  Serial.begin(9600);
  
  // Define board pins
  pinMode(LED_BUILTIN, OUTPUT);
  pinMode(PIR, INPUT);

  attachInterrupt(PIR, pirSensorCallback, CHANGE);

  // Setup BLE service
  blePeripheral.setLocalName("GEN");
  blePeripheral.setAdvertisedServiceUuid(genuinoService.uuid());
  blePeripheral.addAttribute(genuinoService);
  blePeripheral.addAttribute(detectionCharacteristic);
  blePeripheral.addAttribute(sensorCharacteristic);
  blePeripheral.addAttribute(tempCharacteristic);
  blePeripheral.addAttribute(tempAvgCharacteristic);
  blePeripheral.addAttribute(humidCharacteristic);
  blePeripheral.addAttribute(recordCharacteristic);

  // Set initial states to 0 = no alarm
  detectionCharacteristic.setValue(0);
  sensorCharacteristic.setValue(0);
  tempCharacteristic.setValue(19.0);
  tempAvgCharacteristic.setValue(19.0);
  humidCharacteristic.setValue(19.0);
  recordCharacteristic.setValue('0');

  // Begin BLE service
  blePeripheral.begin();

  // Start a 30-second timer
  CurieTimerOne.start(30000000, &halfMinuteTimer);

  tempSensor.begin();
}


// Interrupt callback: reads temperature and adds it to tempArray, then updates average
void halfMinuteTimer() {

  float newT;
  
  newT = tempSensor.readTemperature();
  
  if(!isnan(newT)) {

    // Temperature correcting (calibrated with a fridge :)
    newT = newT - 5;
    tempCharacteristic.setValue(newT);
    addToTempArray(newT);
    currentTempAverage = calculateAverage(tempArray);
    tempAvgCharacteristic.setValue(currentTempAverage);

    if(detectionState != 3) {
      if((currentTempAverage > MAX_TEMP || currentTempAverage < MIN_TEMP) && detectionState == 0) {
        sensorState |= 1 << 3;
        detectionState = 1;
      }
      else {
        sensorState &= ~(1 << 3);
      }
    }
  }

  float newH = tempSensor.readHumidity();
  if(!isnan(newH)) {
    humidCharacteristic.setValue(newH);
  }
}

// Callback for PIR interrupt
void pirSensorCallback() {

  if(detectionState < 2) {
    if(digitalRead(PIR) == HIGH && (millis() - lastDetectionTime < SUSPICIOUS_MOVEMENT_TIME)) {
      detectionState = 1;
      detectionCharacteristic.setValue(detectionState);
      sensorState |= 1 << 0;
      
      lastDetectionTime = millis();

      Serial.println(detectionCharacteristic.value());
    }
    else if(digitalRead(PIR) == HIGH) {
      lastDetectionTime = millis();
    }
    
    else {
      if(detectionState == 2) { 
        detectionState = 0;
        sensorState = 0;
      }
    }
  }
}

// Returns an average of a given float array
float calculateAverage(float tempArray[]) {
  int i = 0;
  float sum = 0;

  while(tempArray[i]) {
    sum += tempArray[i];
    i++;
  }

  return sum/i;
}

// Adds new temperature reading to array of temps for averaging
void addToTempArray(float newTemp) {
  
  static int i = 0;
  
  if(i < TEMP_ARRAY_LENGTH) {
    tempArray[i] = newTemp;
  }
  else {
    i = 0;
    tempArray[i] = newTemp;
  }
  i++;
}

bool isCentralRecording(BLECentral * central) {
  
  if(central) {
    if(recordCharacteristic.written()) {
      if(recordCharacteristic.value() == '2') {
        detectionState = 2;
        detectionCharacteristic.setValue(2);
      }
      else if(recordCharacteristic.value() == '3') {
        detectionState = 3;
        detectionCharacteristic.setValue(3);
      }
      else if(recordCharacteristic.value() == '0') {
        detectionState = 0;
        detectionCharacteristic.setValue(0);
      }
    }
  }

  // If central set detection state 2 (recording), light up LED
  if(detectionState == 2) {
    digitalWrite(LED_BUILTIN, HIGH);
    return true;
  }
  else digitalWrite(LED_BUILTIN, LOW);

  return false;
}

// Main loop
void loop() {

  // Look for connected central device
  BLECentral central = blePeripheral.central();

  // Check if central node has begun recording
  isCentralRecording(&central);
  sensorCharacteristic.setValue(sensorState);

  delay(500); 

  if(millis() - lastDetectionTime > COOLDOWN_TIME && detectionState == 2) {
    detectionState = 0;
    sensorState = 0;
    detectionCharacteristic.setValue(0);
    recordCharacteristic.setValue('0');
  }
}

# -*- coding: utf-8 -*-
#
# Raspberry Pi3 thread module for RBSS
# All threaded functions can be found here
# Mikko Mäkelä
#

import threading, time, datetime
import logger, BLE, dbops, alarmhandler, config

# Worker class for threading functionality
class Worker(threading.Thread):

    def __init__(self, threadName, methodName, delayTime, bsobject = None):

        threading.Thread.__init__(self)

        # Set thread name
        self.name = threadName

        # Make sure the called method name exists and attach it to the variable
        try:
            self.method = globals()[methodName]

        except Exception as e:
            logger.log('WORKER', str(e))
            return

        # Set delay and Bluetooth Support -object
        self.delayTime = delayTime
        self.bsobj = bsobject

        # Set member variable used to signal thread exit
        self.exitThread = False

        logger.log('WORKER', ('Created a new thread (%s)' % self.name))

    # Start thread
    def run(self):

        try:

            # Start thread by method name
            self.method(self, self.delayTime, self.bsobj)

            # Log after thread exit
            logger.log('WORKER', ('Exiting thread (%s)' % self.name))

        except Exception as e:
            logger.log('WORKER (thread run())', str(self.name + ', ' + str(e)))

    # Set threadExit to True -> thread exits (duh)
    def exit(self):
        self.exitThread = True


# Temperature reader function
def tempReader(t, delay, bs):

    logger.log('WORKER', ('Started a new thread (%s)' % t.name))

    # Loop until told to stop
    while not t.exitThread:

        # Read temperatures and humidities from connected nodes
        try:
            temps = bs.readTemps()
            humids = bs.readHumidities()

            # Print data to console
            bs.printTemps()

            # Write collected data to database on server
            # Change 'OK' to actual node health when done
            for uuid, value in temps.iteritems():

                if dbops.writeStatusToDB(uuid, value, humids[uuid], 'OK'):
                    print('Status written to database.')

                    if config.alarmState is 'OFF':
                        print('SYSTEM OFFLINE - waiting for calls...')

                    elif config.alarmState is 'ON':
                        print('SYSTEM ONLINE - monitoring activity...')

        except Exception as e:
            logger.log('WORKER (tempreader)', str(e))

        # Sleep until next round (one second at a time to remain responsive)
        for i in range(delay):
            time.sleep(1)
            if t.exitThread:
                break

# Alarm checker
def alarmReader(t, delay, bs):

    logger.log('WORKER', ('Started a new thread (%s)' % t.name))

    # Loop until told to stop
    while not t.exitThread:

        # Read alarm & sensor status
        if config.currentAlarmStatus < 2:
            alarms = bs.readAlarms()

            for node in alarms:

                try:
                    if alarms[node]['alarm'] == 1:
                        print('New alarm detected, node %s, sensor %s'
                                % (str(node), str(alarms[node]['sensor'])))

                        # Handle the alarm
                        alarmhandler.handleNewAlarm(node, alarms[node]['sensor'], bs)

                except Exception as e:
                    logger.log('WORKER (alarmreader)', str(e))

            # Sleep for a while (one second at a time)
            for i in range(delay):

                time.sleep(1)

                if t.exitThread:
                    break

        while config.currentAlarmStatus == 3:
            time.sleep(1)

        if config.currentAlarmStatus == 2:
            bs.switchAlarm()


# Node health checker (not done yet)
def healthCheck(delay, bs, t):

    # Loop until told to stop
    while not t.exitThread:

        # Sleep for a while (one second at a time)
        for i in range(delay):

            time.sleep(1)

            if t.exitThread:
                break

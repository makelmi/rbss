# -*- coding: utf-8 -*-

# Raspberry Pi3 CALLER module for RBSS
# Mikko Mäkelä
#
# Uses PJSUA libraries for SIP

from __future__ import print_function
import sys
import pjsua
import logger, config

currentCall = None
firstRun = True
buddyList = []

# PJSUA Call callback
class RBSSCallCallback(pjsua.CallCallback):

    def __init__(self, call=None):
        pjsua.CallCallback.__init__(self, call)

    def on_state(self):
        global currentCall
        text = 'Call ' + self.call.info().state_text
        logger.log('CALLER', text)

        if self.call.info().state == pjsua.CallState.DISCONNECTED:
            currentCall = None
            print('Call disconnected')

    def on_media_state(self):

        if self.call.info().media_state == pjsua.MediaState.ACTIVE:

            # Connect call without media (reserved for recording)
            callSlot = self.call.info().conf_slot


# PJSUA Account callback
class RBSSAccountCallBack(pjsua.AccountCallback):

    def __init__(self, account=None):
        pjsua.AccountCallback.__init__(self, account)

    def on_incoming_call(self, call):
        global currentCall
        if currentCall is not None:
            call.answer(486, 'Busy')
            return

        callerURI = str(call.info().remote_uri)

        print('Incoming call from %s, confirming...' % callerURI)

        currentCall = call
        callCb = RBSSCallCallback(currentCall)
        currentCall.set_callback(callCb)


        if str(call.info().remote_uri) in config.acceptedURIs:

            call.answer(200, 'OK')
            currentCall.hangup()
            currentCall = None

            if config.alarmState is 'ON':
                config.alarmState = 'OFF'

            else:
                config.alarmState = 'ON'

            print('Setting alarm %s' % config.alarmState)

            switchPresence()

            msg = 'Alarm system (%s) set %s' % (
                config.RBSS_SYSTEM_FRIENDLY_NAME,
                config.alarmState)

            print('Informing user by IM...', end='')

            if sendMessage(callerURI, msg):
                print('OK')
            else:
                print('Failed')

        else:
            call.answer(403, 'User not recognized')
            currentCall = None
            print('Unauthorized caller')

    def on_incoming_subscribe(self, buddy):

        print('Incoming presence subscription...')
        if buddy:
            print('Subscribe request approved.')
            return(200, None)


    # Print SIP registration status code
    def on_reg_state(self):
        print(
        'SIP registration complete, status = %s(%s)' %
        (self.account.info().reg_status, self.account.info().reg_reason)
        )

class RBSSBuddyCallback(pjsua.BuddyCallback):
    def __init__(self, buddy=None):
        pjsua.BuddyCallback.__init__(self, buddy)

    def on_state(self):
        print('User %s is now "%s"' % (self.buddy.info().uri, self.buddy.info().online_text))


    def on_pager_status(self, body, im_id, code, reason):
        if code >= 300:
            print('Message delivery failed for message')
            print('"%s" to "%s": %s' % (body, self.buddy.info().uri, reason))


# Place a call to given SIP URI
def newCall(uri):
    global account, currentCall
    try:
        print('Calling user %s' % uri)
        currentCall =  account.make_call(uri, cb=RBSSCallCallback())

    except pjsua.Error, e:
        logger.log('CALLER', str(e))
        return

def on_pager(self, mime_type, body):
    print('Instant message from', self.buddy.info().uri)
    print(body)


# Send alarm message to all users
def sendAlarmMessageToAll():

    print('Sending a message to all users...', end='')

    for b in buddyList:
        try:
            b.send_pager(config.messageText)
        except Exception as e:
            logger.log('CALLER', str(e))

    for b in buddyList:
        try:
            b.send_pager(config.recordingUrl)
        except Exception as e:
            logger.log('CALLER', str(e))

    config.messageText = ''
    config.recordingUrl = ''

    print('OK')


# Send an instant message to given SIP URI
def sendMessage(uri, message):

    for b in buddyList:
        if b.info().uri == uri:
            b.send_pager(message)
            return True

        else:
            temp = account.add_buddy(uri, cb=RBSSBuddyCallback())
            temp.send_pager(message)
            return True
    return False


# Delete library instace and clear calls
def exitCaller():
    global transport, lib, currentCall
    del currentCall
    transport = None
    lib.destroy()
    lib = None


# Switch account presence (CSIPSimple subscription is XML-format and won't work)
def switchPresence():
    global account
    presenceText = 'Alarm ' + config.alarmState
    account.set_presence_status(True, 0, presenceText)

if firstRun:
    try:
        # Create a pjsua library instance
        lib = pjsua.Lib()

        lib.init()

        transport = lib.create_transport(pjsua.TransportType.UDP,
                                        pjsua.TransportConfig(55555))

        print('Listening on %s:%i' % (transport.info().host, transport.info().port))

        lib.start()

        accountConfig = pjsua.AccountConfig('keimo.org', config.RBSS_USERNAME, config.RBSS_SIP_PASSWORD)

        accountCallback = RBSSAccountCallBack()
        account = lib.create_account(accountConfig, cb=accountCallback)

        userURI = 'sip:ox@keimo.org'
        buddy = account.add_buddy(userURI, cb=RBSSBuddyCallback())
        buddy.subscribe()
        buddyList.append(buddy)

        account.set_basic_status(True)
        switchPresence()


    except pjsua.Error, e:
        logger.log(moduleName, str(e))
        del currentCall
        lib.destroy()
        lib = None

    firstRun = False

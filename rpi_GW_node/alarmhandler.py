# RBSS Alarm handler module

from __future__ import print_function
import dbops, config, logger
import os, pysftp
from subprocess import call

avconv = 'avconv -f video4linux2 -r 15 -i /dev/video0 -f alsa -i plughw:U0x46d0x825,0 -ar 22050 -ab 64k -strict experimental -acodec aac -vf "drawtext=fontfile=/usr/share/fonts/truetype/dejavu/DejaVuSans-Bold.ttf:text=\'\%d/%m/%y %H\:%M\:%S\':fontcolor=white@0.8: x=7: y=460" -vcodec mpeg4 -t 5 '

# Handle a new alarm:
# Increment alarmId, begin recording, place call to user, write to database
def handleNewAlarm(node, sensor, bs):

    # Open alarmcounter file and update counter to current alarmId
    with open('alarmcounter.data') as f:

        for line in f:
            data = line.split()

            alarmId = int(data[0]) + 1

    # Update status on the alarming node
    bs.writeAlarmCheck(node)

    # Set the flag for main thread to call user
    config.placeCall = True

    # Create a filename for this recording
    recordingFilename = node + '_' + str(alarmId) + '.mp4'

    # Merge the command and the filename
    captureCommand = avconv + recordingFilename

    print('Starting video recording')

    # Start the avconv process
    try:
        devnull = open(os.devnull, 'w')
        call(captureCommand, shell=True, stdout=devnull, stderr=devnull)

    except Exception as e:
        logger.log('ALARMHANDLER', str(e))

    print('Recording finished.')
    print('Uploading to server...', end='')

    # Open SFTP connection to server and upload the file
    with pysftp.Connection('keimo.org',
                            username=config.RBSS_SFTP_USERNAME,
                            password=config.RBSS_SFTP_PASSWORD) as sftp:

        with sftp.cd('/var/www/html/recordings'):
            sftp.put(recordingFilename)

    print('OK')
    print('Deleting local file...', end='')

    # Remove the local video file
    try:
        os.remove(recordingFilename)
        print('OK')
    except Exception as e:
        logger.log('ALARMHANDLER', str(e))

    print('Writing alarm to database...', end='')
    # Write alarm to database and rewrite alarmcounter
    if dbops.writeAlarmToDB(node, sensor, alarmId, recordingFilename):
        with open('alarmcounter.data', 'w') as f:
            f.seek(0)
            f.write(str(alarmId))
            f.truncate()
        print('OK')

    url = 'https://keimo.org/recordings/' + recordingFilename

    # Construct a message to send all system users
    # and place it on a shared variable
    config.messageText = 'New alarm (%s: %s, %s), recording available @ ' % (
                        config.RBSS_SYSTEM_FRIENDLY_NAME,
                        config.RBSS_NODE_NAMES[node],
                        config.RBSS_SENSOR_NAMES[sensor]
                        )

    config.recordingUrl = url

    # Set flag to send instant message
    config.sendIM = True

    return True

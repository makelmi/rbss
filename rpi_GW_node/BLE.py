# -*- coding: utf-8 -*-

# Raspberry Pi3 BLE module for RBSS
# Mikko Mäkelä
#
# Uses Ian Harvey's bluepy library for BLE:
# https://github.com/IanHarvey/bluepy

import logger, config
from bluepy.btle import Peripheral, Scanner, DefaultDelegate, ScanEntry
from struct import *

# Known service UUID substring, last 4 bytes distinguish between sensors
SERVICE_UUID = 'a699b6ce-3b10-401a-834f-d1cee300'


# Known characteristic UUIDs
CHAR_UUID_DETECT = 'a699b6ce-3b10-401a-834f-d1cee304771f'
CHAR_UUID_SENSOR = 'a699b6ce-3b10-401a-834f-d1cee304772f'
CHAR_UUID_TEMP = 'a699b6ce-3b10-401a-834f-d1cee304773f'
CHAR_UUID_TEMPAVG = 'a699b6ce-3b10-401a-834f-d1cee304774f'
CHAR_UUID_HUMIDITY = 'a699b6ce-3b10-401a-834f-d1cee304775f'
CHAR_UUID_RECORD = 'a699b6ce-3b10-401a-834f-d1cee304776f'


# Characteristic data types (for struct unpack)
UUIDS = {CHAR_UUID_DETECT: 'i',
        CHAR_UUID_SENSOR: 'i',
        CHAR_UUID_TEMP: 'f',
        CHAR_UUID_TEMPAVG: 'f',
        CHAR_UUID_HUMIDITY: 'f',
        CHAR_UUID_RECORD: 'c'}


# Characteristic UUID to description mapping
UUID_NAMES = {CHAR_UUID_DETECT: 'Alarm flag',
                CHAR_UUID_SENSOR: 'Activated sensor',
                CHAR_UUID_TEMP: 'Current temperature reading',
                CHAR_UUID_TEMPAVG: 'Current average temperature',
                CHAR_UUID_HUMIDITY: 'Current humidity reading',
                CHAR_UUID_RECORD: 'Recording status'}


# bluepy DefaultDelegate class override
class ScanDelegate(DefaultDelegate):
    def __init__(self):
        DefaultDelegate.__init__(self)

    def handleDiscovery(self, dev, isNewDev, isNewData):
        if isNewDev:
            logger.log('BLE', ('Discovered device ' + dev.addr))


# Support class for BLE connectivity
class BluetoothSupport(object):

    def __init__(self):

        # List of found nodes
        self.genNodes = []

        # List of connected Peripheral objects
        self.peripherals = []

        # Dictionary mapping {UUID: characteristic handle}
        self.followedHandles = {}

        # Dictionary mapping {characteristic handle: service UUID}
        self.followedHandlesServices = {}

        # Dictionary holding temperature data
        self.temperatures = {}

        # Dictionary holding avg. temperature data
        self.avgTemps = {}

        # Dictionary holding humidity data
        self.humidities = {}


    # Initial connection method
    # Scan, connect, get peripheral information
    def initialConnect(self):
        print('Scanning devices...')

        # Scan with bluepy ScanDelegate (5s scan time)
        scanner = Scanner().withDelegate(ScanDelegate())
        devicesInRange = scanner.scan(5)


        # Print no devices found
        if not devicesInRange:
            t = 'Scan complete. No devices within range.'
            logger.log('BLE', t)
            print(t)


        # Print found device info to syslog and add to node list
        else:
            for d in devicesInRange:
                logger.log('BLE', ('Device found: %s (%s), RSSI=%d dB' % (d.addr, d.addrType, d.rssi)))

            for (adtype, desc, value) in d.getScanData():
                logger.log('BLE', (' %s = %s' % (desc, value)))

                if value == 'GEN':
                    logger.log('BLE', 'Genuino-node found')
                    print('Gen node found: %s') % d.addr
                    self.genNodes.append(d)


        # Loop through node list, create Peripheral objects and add to list
        # Creating a Peripheral object also connects automatically
        for n in self.genNodes:
            try:
                self.peripherals.append(Peripheral(n.addr))

            except BTLEException as e:
                logger.log('BLE', str(e))


        # Loop through connected peripherals and populate handle dictionaries
        # {characteristic UUID: characteristic handle},
        # {characteristic handle: service UUID}
        for p in self.peripherals:

            services = p.getServices()
            characteristics = p.getCharacteristics()

            for c in characteristics:
                if str(c.uuid) in UUIDS:
                    self.followedHandles[str(c.uuid)] = c.getHandle()

                    print('Following UUID %s, %s') % (c.uuid, UUID_NAMES[str(c.uuid)])

                    for s in services:
                        if str(s.uuid).find(SERVICE_UUID) != -1:
                            self.followedHandlesServices[c.getHandle()] = str(s.uuid)


    # Return all connected peripherals
    def getPeripheralList(self):
        return self.peripherals


    # Loop through all GEN peripherals and reconnect
    def checkConnections(self):

        for p in self.peripherals:

            if p is not None:
                try:
                    p.connect(p.addr)
                except Exception as e:
                    logger.log('BLE', str(e))


    # Print temperature (current and average) and humidity readings
    # from all connected nodes
    def printTemps(self):

        for u, t in self.temperatures.iteritems():
            print('Node UUID: %s' % str(u))
            print('Node temperature: %fC' % t)
            print('Node average temperature %fC' % self.avgTemps[u])
            print('Node humidity: %f%%') % (self.humidities[u])


    # Returns a dictionary of current temperatures on all connected nodes
    def readTemps(self):

        tempDict = {}

        # Loop all peripherals and try to reconnect
        for p in self.peripherals:

            try:
                p.connect(p.addr)
            except Exception as e:
                logger.log('BLE (connect)', str(e))

            # Loop all followed characteristics from current peripheral
            for u, h in self.followedHandles.iteritems():

                # Read temperature data
                if u == CHAR_UUID_TEMP:

                    data = p.readCharacteristic(h)

                    # Unpack temperature float
                    try:
                        temp = unpack(UUIDS[u], data)

                    except Exception as e:
                        logger.log('BLE', str(e))

                    # Add to dictionary {Service UUID: temperature}
                    tempDict[self.followedHandlesServices[h]] = temp


                elif u == CHAR_UUID_TEMPAVG:

                    data = p.readCharacteristic(h)

                    # Unpack temperature float
                    try:
                        tempAvg = unpack(UUIDS[u], data)

                    except Exception as e:
                        logger.log('BLE', str(e))

                    # Add to dictionary {Service UUID: temperature avg.}
                    self.avgTemps[self.followedHandlesServices[h]] = tempAvg

        self.temperatures = tempDict

        return tempDict

    # Returns a dictionary of current humidities on all connected nodes
    # UUID: humidity
    def readHumidities(self):

        humidDict = {}

        # Loop all peripherals and try to reconnect
        for p in self.peripherals:

            try:
                p.connect(p.addr)
            except Exception as e:
                logger.log('BLE (connect)', str(e))

            # Loop all followed characteristics from current peripheral
            for u, h in self.followedHandles.iteritems():

                # Read humidity data
                if u == CHAR_UUID_HUMIDITY:

                    data = p.readCharacteristic(h)

                    # Unpack humidity float
                    try:
                        humidity = unpack(UUIDS[u], data)

                    except Exception as e:
                        logger.log('BLE', str(e))

                    # Add to dictionary {Service UUID: humidity}
                    humidDict[self.followedHandlesServices[h]] = humidity

        self.humidities = humidDict

        return humidDict


    # Read alarms from all connected sensor nodes and return a dictionary
    # {UUID: {alarmStatus, sensorStatus}}
    def readAlarms(self):

        alarmStatusDict = {}

        # Loop all peripherals and try to reconnect
        for p in self.peripherals:

            # Loop all followed characteristics from current peripheral
            for u, h in self.followedHandles.iteritems():

                # Read alarm status
                # 0 = No alarm, 1 = Unchecked alarm, 2 = Checked alarm, 3 = Off
                if u == CHAR_UUID_DETECT:

                    data = p.readCharacteristic(h)

                    # Unpack integer
                    try:
                        alarmStatus = unpack(UUIDS[u], data)

                    except Exception as e:
                        logger.log('BLE (alarm unpack)', str(e))

                    # Create a nested dictionary
                    alarmStatusDict[self.followedHandlesServices[h]] = {}

                    # Add alarm status: {UUID: {'alarm': alarmStatus}}
                    alarmStatusDict[self.followedHandlesServices[h]]['alarm'] = alarmStatus[0]


            for u, h in self.followedHandles.iteritems():

                if u == CHAR_UUID_SENSOR:

                    data = p.readCharacteristic(h)

                    try:
                        sensorStatus = unpack(UUIDS[u], data)
                    except Exception as e:
                        logger.log('BLE (sensor unpack)', str(e))

                    # Add alarm status: {UUID: {'sensor': sensorStatus}}
                    alarmStatusDict[self.followedHandlesServices[h]]['sensor'] = sensorStatus[0]

        return alarmStatusDict


    # Returns a dictionary of 3min average temps from all connected nodes
    # {UUID: averageTemp}
    def readAvgTemps(self):

        averageTempDict = {}

        for p in self.peripherals:

            for u, h in self.followedHandles.iteritems():

                if u == CHAR_UUID_TEMPAVG:

                    data = p.readCharacteristic(h)

                    try:
                        temp = unpack(UUIDS[u], data)

                    except Exception as e:
                        logger.log('BLE', str(e))

                    averageTempDict[self.followedHandlesServices[h]] = temp

        self.avgTemps = averageTempDict

        return averageTempDict


    # Write data to characteristic
    def writeAlarmCheck(self, pUUID):

        for p in self.peripherals:

            for s in p.getServices():

                if s.uuid == pUUID:

                    for c in s.getCharacteristics():

                        if c.uuid == CHAR_UUID_RECORD:

                            try:
                                if c.write('2', withResponse=True):
                                    logger.log('BLE (write)', 'Node status set to 2')

                            except Exception as e:
                                logger.log('BLE (write)', str(e))


    def switchAlarm(self):

        for p in self.peripherals:

            for s in p.getServices():

                for c in s.getCharacteristics():

                    if c.uuid == CHAR_UUID_RECORD:

                        try:
                            if config.alarmState is 'ON':
                                if c.write('0', withResponse=True):
                                    logger.log('BLE (write)', 'Node status set to 3')

                            elif config.alarmState is 'OFF':
                                if c.write('3', withResponse=True):
                                    logger.log('BLE (write)', 'Node status set to 3')


                        except Exception as e:
                            logger.log('BLE (write)', str(e))



    # Rescan for devices, not written yet
    def rescan(self):
        return

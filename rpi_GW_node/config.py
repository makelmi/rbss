# Imaginary description of system properties
RBSS_SYSTEM_FRIENDLY_NAME = 'Home'
RBSS_NODE_NAMES = {'a699b6ce-3b10-401a-834f-d1cee3000001': 'Back entrance'}
RBSS_SENSOR_NAMES = ['', 'MOVEMENT', 'TEMPERATURE', 'GYRO']

RBSS_USERNAME = ''
RBSS_SIP_PASSWORD = ''

RBSS_SFTP_USERNAME = ''
RBSS_SFTP_PASSWORD = ''

acceptedURIs = []
acceptedURIs.append('<sip:ox@keimo.org>')
acceptedURIs.append('<sip:ox@128.199.56.81>')

# Flags between alarm thread and main
placeCall = False
sendIM = False

messageText = ''
recordingUrl = ''

# System alarm states (0 = on & no alarm, 1 = on & new alarm, 2 = going off/on, 3 = off)
currentAlarmStatus = 0
alarmState = 'ON'

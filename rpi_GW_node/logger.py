# -*- coding: utf-8 -*-

# Raspberry Pi3 LOG module for RBSS
# Mikko Mäkelä


import logging, logging.handlers

l = logging.getLogger('l')
l.setLevel(logging.DEBUG)

# Get syslog handler
handler = logging.handlers.SysLogHandler(address = '/dev/log')

l.addHandler(handler)

# Add to syslog function
def log(moduleName, logText):

    l.debug('[RBSS ' + moduleName + '] '  + logText)

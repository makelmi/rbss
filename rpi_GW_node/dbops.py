# -*- coding: utf-8 -*-
#
# Raspberry Pi3 database module for RBSS
# Mikko Mäkelä
#

import logger
import datetime
import pymongo

serverString = 'mongodb://128.199.56.81:27017'
moduleName = 'DBOPS'

# Write system status to database @ 128.199.56.81
def writeStatusToDB(nodeUUID, currentTemp, currentHumid, nodeStatus):

    try:
        conn = pymongo.MongoClient(serverString)
        logger.log(moduleName, 'Connected to database')

    except pymongo.errors.ConnectionFailure as e:
        logger.log(moduleName, str(e))
        return False

    timeStamp = '{:%d-%m-%Y %H:%M:%S}'.format(datetime.datetime.now())

    # Choose where to write the document
    db = conn['rbss']
    collection = db['status']

    # Prepare a JSON object to insert into database
    insert = {'uuid': nodeUUID,
            'timestamp': timeStamp,
            'temperature': currentTemp,
            'humidity': currentHumid,
            'status': nodeStatus}

    # Read current system status and add to JSON
    sys = getSystemInfo()

    for key, value in sys.iteritems():
        insert[key] = value

    # Insert data to database
    try:
        collection.insert_one(insert)
        logger.log(moduleName, 'Document successfully written to database.')

    except Exception as e:
        logger.log(moduleName, str(e))
        return False

    return True


# Write alarm info to database @ 128.199.56.81
def writeAlarmToDB(nodeUUID, sensorStatus, alarmId, recording):

    try:
        conn = pymongo.MongoClient(serverString)
        logger.log(moduleName, 'Connected to database')

    except pymongo.errors.ConnectionFailure as e:
        logger.log(moduleName, str(e))
        return False

    timeStamp = '{:%d-%m-%Y %H:%M:%S}'.format(datetime.datetime.now())

    # Choose where to write the document
    db = conn['rbss']
    collection = db['alarms']

    # Prepare a JSON object to insert into database
    insert = {'uuid': nodeUUID,
            'timestamp': timeStamp,
            'sensor': sensorStatus,
            'alarmId': alarmId,
            'recording': recording}

    # Insert data to database
    try:
        collection.insert_one(insert)
        logger.log(moduleName, 'Document successfully written to database')

    except Exception as e:
        logger.log(moduleName, str(e))
        return False

    return True


# Read current system info (uptime, load averages, meminfo)
# Returns a dictionary mapping with keywords:
# 'Uptime' (Seconds, long)
# 'OneMinLoad', 'FiveMinLoad', 'FifteenMinLoad' (float)
# 'MemFreekB', 'MemTotalkB', 'MemAvailablekB' (kilobytes, int)
def getSystemInfo():

    sysInfo = {}

    try:
        with open('/proc/loadavg', 'r') as f:
            data = f.readlines()

            for l in data:
                w = l.split()

            oneMinLoad = float(w[0])
            fiveMinLoad = float(w[1])
            fifteenMinLoad = float(w[2])

            sysInfo['OneMinLoad'] = oneMinLoad
            sysInfo['FiveMinLoad'] = fiveMinLoad
            sysInfo['FifteenMinLoad'] = fifteenMinLoad

        with open('/proc/uptime', 'r') as f:
            data = f.readlines()

            for l in data:
                w = l.split('.')

            utLong = long(w[0])

            sysInfo['Uptime'] = utLong

        with open('/proc/meminfo', 'r') as f:
            data = f.readlines()


            for l in data:
                w = l.split()

                if w[0] == 'MemTotal:':
                    sysInfo['MemTotalkB'] = int(w[1])

                elif w[0] == 'MemFree:':
                    sysInfo['MemFreekB'] = int(w[1])

                elif w[0] == 'MemAvailable:':
                    sysInfo['MemAvailablekB'] = int(w[1])


    except Exception as e:
        logger.log(moduleName, str(e))
        print(str(e))

    return sysInfo

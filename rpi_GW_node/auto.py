#!/usr/bin/python
# -*- coding: utf-8 -*-

# Raspberry Pi3 MAIN module for RBSS
# Mikko Mäkelä

import BLE, caller, logger, worker, config
import time

# TODO & Basic operation
#
# Check periodically for GEN nodes
# Connect to GEN nodes
# Check if opened connections are OK
# Subscribe to notifications from nodes
# Act on alarms ->
# Start recording, write status to alarming node(s), place a call to user
# Get accepted users from database
# Scan bugging from other devices?


# Settings
settings = {'TEMP_READ_INTERVAL': 900,
            'ALARM_READ_INTERVAL': 5,
            'SCAN_TIME': 5}

# Get an instance of BT-support class
bs = BLE.BluetoothSupport()

# Main loop, escape with CTRL-C
def auto():

    # Scan, connect and follow GEN-nodes in range
    bs.initialConnect()

    # If connected to at least one node, create threads to execute tasks
    if bs.getPeripheralList:

        # Create threads
        tempReadThread = worker.Worker('Temperature reader thread','tempReader',
                                        settings['TEMP_READ_INTERVAL'], bs)

        alarmReadThread = worker.Worker('Alarm reader thread','alarmReader',
                                        settings['ALARM_READ_INTERVAL'], bs)

        # Start temperature reader
        tempReadThread.start()

        # Start alarm reader
        alarmReadThread.start()

    # TODO: Else: Create a timer thread checking for connected devices
    else:
        return


    # Loop code, execute until KeyboardInterrupt
    try:
        while True:
            for i in range(10):
                time.sleep(1)
                if config.placeCall:
                    caller.newCall('sip:ox@keimo.org')
                    config.placeCall = False

                if config.alarmState is 'OFF':
                    if config.currentAlarmStatus != 3:
                        config.currentAlarmStatus = 3

                if config.alarmState is 'ON':
                    if config.currentAlarmStatus == 3:
                        config.currentAlarmStatus = 0

                if config.sendIM:
                    caller.sendAlarmMessageToAll()
                    config.sendIM = False



    except KeyboardInterrupt:
        print()
        print('Exiting automatic mode.')

        # Send exit signal to started threads
        tempReadThread.exit()
        alarmReadThread.exit()
        caller.exitCaller()

        # Join threads to main and exit
        tempReadThread.join()
        alarmReadThread.join()

        logger.log('AUTO', 'Threads joined, exiting.')
        return

auto()
